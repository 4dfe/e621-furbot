package de.fourdfe.furbot

import kotlin.system.exitProcess

import me.ivmg.telegram.bot
import me.ivmg.telegram.dispatch
import me.ivmg.telegram.dispatcher.command
import me.ivmg.telegram.dispatcher.telegramError
import me.ivmg.telegram.dispatcher.text
import me.ivmg.telegram.entities.ParseMode
import me.ivmg.telegram.network.fold

import okhttp3.logging.HttpLoggingInterceptor

class TriggerWord(val isExplicit: Boolean, val url: String = "https://e621.net/post/random")

fun main(args: Array<String>) {

    // Get token from arguments
    val apiToken: String = if (args.size == 1) args[0] else ""

    // If empty show usage
    if (apiToken.isEmpty()) usage(exit = true)

    // list of trigger words
    val triggerWords = mapOf(
        "e621" to TriggerWord(isExplicit = true),
        "yiff" to TriggerWord(isExplicit = true),
        "e926" to TriggerWord(isExplicit = false, url = "https://e926.net/post/random"),
        "owo" to TriggerWord(isExplicit = false, url = "https://e926.net/post?tags=owo")
    )

    // create new bot
    val bot = bot {
        token = apiToken
        logLevel = HttpLoggingInterceptor.Level.BASIC
        timeout = 30

        dispatch {
            command("start") { bot, update ->
                // If chat id is missing just return to command
                val chatId = update.message?.chat?.id ?: return@command

                val result = bot.sendMessage(chatId, text = """
                Hello ${ update.message?.chat?.firstName ?: update.message?.from?.firstName ?: "User" },
                this is a bot which fetches random images from https://e621.net.
                Warning: Pictures are mostly NSFW

                This Bot is triggered by the keywords ${getWordList(words = triggerWords)}
                """.trimIndent(), parseMode = ParseMode.MARKDOWN)

                result.fold({
                }, {
                })
            }

            command("tag") { bot, update, args ->
                // If chat id is missing just return to command
                val chatId = update.message?.chat?.id ?: return@command

                if (args.isNotEmpty()) {
                    // Get first arg
                    val firstArg = args[0]

                    if (!firstArg.isBlank()) {
                        bot.sendMessage(chatId, text = """
                        _bounces on ${ update.message?.chat?.firstName ?: update.message?.from?.firstName ?: "User" }_

                        https://e621.net/post?tags=$firstArg

                        This might be explicit and *NSFW*!
                        """.trimIndent(), parseMode = ParseMode.MARKDOWN)
                    }
                } else {
                    bot.sendMessage(chatId, "Usage /tag <Tag>")
                }
            }

            command("help") { bot, update ->
                // If chat id is missing just return to command
                val chatId = update.message?.chat?.id ?: return@command

                bot.sendMessage(chatId, text = """
                Hello ${ update.message?.chat?.firstName ?: update.message?.from?.firstName ?: "User" },

                Available commands are: *tag*
                """.trimIndent(), parseMode = ParseMode.MARKDOWN)
            }

            text { bot, update ->
                // If chat id is missing just return to text
                val chatId = update.message?.chat?.id ?: return@text

                // Convert message to lowercase (return if null)
                val message = update.message?.text?.toLowerCase() ?: return@text

                if (message in triggerWords) {
                    bot.sendMessage(chatId, text = """
                    _bounces on ${ update.message?.chat?.firstName ?: update.message?.from?.firstName ?: "User" }_
                    you said the magic word $message! Here is your reward:

                    ${ triggerWords[message]?.url }
                    _(Telegram may displays a preview from previous results)_

                    ${ if (triggerWords[message]?.isExplicit != false) "This might be explicit and *NSFW*!" else "This is *SFW*" }
                    """.trimIndent(), parseMode = ParseMode.MARKDOWN)
                }
            }

            telegramError { _, telegramError ->
                println(telegramError.getErrorMessage())
            }
        }
    }

    // start bot
    bot.startPolling()
}

fun usage(exit: Boolean = true) {
    println("""
    usage:
        java -jar furbot.jar API_KEY
    """.trimIndent())

    if (exit) exitProcess(0)
}

fun getWordList(words: Map<String, TriggerWord>): String {
    var resultList: MutableList<String> = mutableListOf("")
    resultList.clear()

    words.forEach {
        resultList.add("${it.key} (${ if (it.value.isExplicit) "NSFW" else "SFW" })")
    }

    return resultList.toString()
}